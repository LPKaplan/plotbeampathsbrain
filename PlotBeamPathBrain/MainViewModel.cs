﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using win = System.Windows;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using OxyPlot.Wpf;
using OxyPlot.Annotations;
using VMS.TPS.Common.Model.API;
using VMS.TPS.Common.Model.Types;

namespace PlotBeamPathBrain
{
    public class MainViewModel
    {
        #region Properties
        public IEnumerable<PlanSetup> PlansInScope { get; private set; }
        public IEnumerable<PlanName> PlanCollection { get; set; }
        public PlotModel PlotModel { get; private set; }
        public string ChosenStructureId { get; set; }
        public IEnumerable<Structure> Structures { get; private set; }
        #endregion

        // Constructor
        public MainViewModel(ScriptContext context)
        {
            // Set properties
            PlansInScope = GetPlansInScope(context);
            PlanCollection = GetPlanCollection(PlansInScope);
            Structures = GetPlanStructures(context);
            PlotModel = CreatePlotModel();
        }

        #region Private methods for setting properties
        // Get a list of all plans in the current context
        private IEnumerable<PlanSetup> GetPlansInScope(ScriptContext context)
        {
            var plans = context.PlansInScope != null
                ? context.PlansInScope : null;

            return plans.Where(p => p.IsDoseValid);
        }

        // Get a list of all structures in the currently loaded structure set
        private IEnumerable<Structure> GetPlanStructures(ScriptContext context)
        {
            var plan = context.PlanSetup != null ? context.PlanSetup : null;
            return plan.StructureSet != null
                ? plan.StructureSet.Structures : null;
        }

        // Make list of PlanName objects. The PlanName class implements INotifyPropertyChanged. 
        // This is so that the script can tell if a plan is checked or unchecked. 
        // Used to uncheck all plans when switching between plot views.
        private IEnumerable<PlanName> GetPlanCollection(IEnumerable<PlanSetup> plansInScope)
        {
            var List = new List<PlanName>();
            foreach (var plan in plansInScope)
            {
                var planName = new PlanName() { Name = plan.Id };
                List.Add(planName);
            }
            return List;
        }

        // Create an empty PlotModel
        private PlotModel CreatePlotModel()
        {
            PlotModel plotModel = new PlotModel();
            plotModel.Background = OxyColors.LightSlateGray;
            plotModel.PlotAreaBackground = OxyColors.White;
            return plotModel;
        }
        #endregion

        #region Private methods for manipulating the PlotModel

        // Find a plan series
        private OxyPlot.Series.Series FindSeries(string id)
        {
            return PlotModel.Series.FirstOrDefault(x =>
            x.Tag.ToString().Contains(id));
        }

        // Find an annotation (line or text in the plot)
        private OxyPlot.Annotations.Annotation FindAnnotation(string id)
        {
            return PlotModel.Annotations.FirstOrDefault(x =>
            (string)x.Tag == id);
        }

        // Find index of plan in PlansInScope, used for placement of boxes and bars on the x-axis
        private double PlanIndex(PlanSetup plan)
        {
            List<PlanSetup> PlanList = PlansInScope.ToList();
            string id = plan.Id;

            return (double)PlanList.FindIndex(pln => pln.Id == id);
        }

        // Update plot when data is added or removed
        private void UpdatePlot()
        {
            PlotModel.InvalidatePlot(true);
        }
        #endregion

        #region Public general methods

        // Set up the correct axes
        public void ShowAxes()
        {
            // Get IDs of plans in scope
            IEnumerable<string> planIDs = PlansInScope.Select(p => p.Id);

            

            // Add two linear axes
            OxyPlot.Axes.CategoryAxis xaxis = new OxyPlot.Axes.CategoryAxis
            {
                Title = "Plan ID",
                TitleFontSize = 18,
                TitleFontWeight = FontWeights.Bold,
                AxisTitleDistance = 15,
                FontSize = 16,
                MajorGridlineStyle = LineStyle.Solid,
                MinorGridlineStyle = LineStyle.Solid,
                Position = AxisPosition.Bottom,
                Key = "xaxis"
            };
            xaxis.Labels.AddRange(planIDs);


            OxyPlot.Axes.LinearAxis yaxis = new OxyPlot.Axes.LinearAxis
            {
                Title = "Volume of healthy brain traversed [cc]",
                TitleFontSize = 18,
                TitleFontWeight = FontWeights.Bold,
                AxisTitleDistance = 15,
                FontSize = 16,
                MajorGridlineStyle = LineStyle.Solid,
                MinorGridlineStyle = LineStyle.Solid,
                Position = AxisPosition.Left,
                Key = "yaxis"
            };

            PlotModel.Axes.Add(xaxis);
            PlotModel.Axes.Add(yaxis);


            // No legend
            PlotModel.IsLegendVisible = false;

            // Set a title
            PlotModel.Title = "Volume of healthy brain traversed by beams";
        }

        

        // Add series when a plan is checked
        public void AddPlanItem(PlanSetup plan)
        {
            if (plan.PlanType != PlanType.ExternalBeam_Proton)
            {
                win.MessageBox.Show("Plan must be a proton plan!");
                return;
            }

            // Cast as ionplan
            IonPlanSetup ionPlan = (IonPlanSetup)plan;

            // If no beams are defined
            if (ionPlan.IonBeams.Count() == 0)
            {
                win.MessageBox.Show("The plan has no defined ion beams.");
                return;
            }
            // If no structure is chosen
            else if (ChosenStructureId == null)
            {
                win.MessageBox.Show("Please define a healthy brain structure.");
                return;
            }
            // If the checked plan does not have the chosen structure in its structure set:
            else if (!plan.StructureSet.Structures.Any(str => (str.Id == ChosenStructureId & str.IsEmpty == false)))
            {
                win.MessageBox.Show("The plan is not connected to a structure with the chosen ID, or the structure has no segment.");
                return;
            }
            // If everything is OK add series to the plot
            else
            {
                Structure structure = ionPlan.StructureSet.Structures.First(s => s.Id == ChosenStructureId);
                AddBeamPathSeries(ionPlan, structure);

                UpdatePlot();
            }
        }

        // Remove series from plot when box is unchecked
        public void RemovePlanItem(PlanSetup plan)
        {
            while (PlotModel.Series.Any(ser => (string)ser.Tag == plan.Id))
            {
                var series = FindSeries(plan.Id);
                PlotModel.Series.Remove(series);
            }
            while (PlotModel.Annotations.Any(ser => (string)ser.Tag == plan.Id))
            {
                var annotation = FindAnnotation(plan.Id);
                PlotModel.Annotations.Remove(annotation);
            }
            UpdatePlot();
        }

        // Clear previous plot model
        public void OnClear()
        {
            PlotModel.Axes.Clear();
            PlotModel.Series.Clear();
            PlotModel.InvalidatePlot(true);
        }

        // Clear all plot series when input is changed
        public void RemoveAllSeries()
        {
            PlotModel.Series.Clear();
            PlotModel.InvalidatePlot(true);
        }

        // Export plot as pdf
        public void ExportPlotAsPdf(string filePath)
        {
            using (var stream = File.Create(filePath))
            {
                PdfExporter.Export(PlotModel, stream, 600, 400);
            }
        }

        // Export plotted data as txt file
        public void ExportDataAsTxt(string filePath)
        {
            using (var sw = new StreamWriter(filePath))
            {
                foreach (var ser in PlotModel.Series)
                {
                    if (OxyPlot.TypeExtensions.Equals(ser.GetType(), new OxyPlot.Series.ColumnSeries().GetType()))
                    {
                        OxyPlot.Axes.CategoryAxis xaxis = (OxyPlot.Axes.CategoryAxis)PlotModel.Axes.First(ax => ax.GetType() == new OxyPlot.Axes.CategoryAxis().GetType()); // get category x axis
                        var dataSer = (OxyPlot.Series.ColumnSeries)ser;
                        if (dataSer.Items.Any(it => (it.Value < 0 | it.Value >= 0)))
                        {
                            sw.WriteLine(string.Format("#{0}", ser.Title));
                            sw.WriteLine(string.Format("#CategoryIndex\tValue"));
                            foreach (var item in dataSer.Items)
                            {
                                sw.WriteLine(string.Format("{0}\t{1}", xaxis.Labels.ElementAt(item.CategoryIndex), item.Value));
                            }
                        }
                    }
                }
            }
        }
        #endregion

        #region Create series methods

        // Add plan's series to the plot. The way OxyPlot is set up each stack needs to be its own series.
        private void AddBeamPathSeries(IonPlanSetup ionPlan, Structure structure)
        {
            // calculate beam path lengths through structure
            var beamPaths = GetBeamPathROIVolsBrain(ionPlan, structure);

            List<OxyColor> colors = new List<OxyColor> { OxyColors.LightBlue, OxyColors.MediumBlue, OxyColors.Cyan, OxyColors.DarkBlue, OxyColors.DarkGray, OxyColors.DarkGreen };

            int count = 0;
            foreach (var beam in ionPlan.Beams)
            {
                // define nice color
                OxyColor color;
                if (count < colors.Count())
                {
                    color = colors[count];
                }
                else
                {
                    color = colors[count % colors.Count()];
                }

                // don't consider setup fields
                if (beam.IsSetupField)
                {
                    continue;
                }

                PlotModel.Series.Add(CreateStackedColumnSeries(ionPlan, structure, beamPaths[count], beam.Id, color));
                count++;
            }
        }

        // create the stacked column series
        private OxyPlot.Series.Series CreateStackedColumnSeries(PlanSetup plan, Structure structure, double val, string title, OxyColor color)
        {
            // Initiate seríes
            var series = new OxyPlot.Series.ColumnSeries();
            series.IsStacked = true;
            series.Tag = plan.Id;
            series.Title = title;
            series.XAxisKey = "xaxis";
            series.FillColor = color;

            // calculate beam CAX path and create columnitem
            var item = new OxyPlot.Series.ColumnItem { Value = val, CategoryIndex = (int)PlanIndex(plan) };
            series.Items.Add(item);

            return series;
        }

        // Get volume in beam path
        public double[] GetBeamPathROIVolsBrain(IonPlanSetup plan, Structure structure)
        {
            // initialize return array of length (number of beams)
            double[] returnArray = new double[plan.Beams.Count(b => !b.IsSetupField)];

            // loop over beams
            int beamCount = 0;
            foreach (var beam in plan.Beams)
            {
                IonBeam ionBeam = (IonBeam)beam;
                Structure beamTarget = ionBeam.TargetStructure;

                // Get position of source
                double beamAngle = ionBeam.IonControlPoints.First().GantryAngle;
                VVector beamSourcePosition = ionBeam.GetSourceLocation(beamAngle);

                
                // define direction parallel to source-isocenter vector. Direction: isocenter to source
                VVector sourceToIsoCenterVector = beamSourcePosition - ionBeam.IsocenterPosition;
                VVector unitVector = sourceToIsoCenterVector / sourceToIsoCenterVector.Length; // unit length should be 1mm

                // initialize point in plane
                VVector planePoint = new VVector();

                // Define plane of search start points perpendicular to the source-isocenter line
                // Eq for a plane: a(x-x0) + b(y-y0) + c(z-z0) = 0. (a,b,c): normal vector. (x0,y0,z0): point in plane

                // The plane should lie behind the target structure. Move back along the source to isocenter line until no longer inside Brain structure

                for (int i = 1; i < 1000; i++) // I guess after a meter you should be outside the body contour
                {
                    VVector point = ionBeam.IsocenterPosition - i * unitVector;
                    if (!structure.IsPointInsideSegment(point) & !beamTarget.IsPointInsideSegment(point))
                    {
                        planePoint = point;
                        break;
                    }
                }

                // find random vector in the plane
                VVector randomPlanePoint = new VVector(planePoint.x, planePoint.y + 5 / unitVector.y, planePoint.z - 5 / unitVector.z);
                VVector planeVector1 = randomPlanePoint - planePoint;
                VVector planeUnitVector1 = planeVector1 / planeVector1.Length;

                // find second plane vector. Cross product of unit normal vector and unit first plane vector gives orthogonal unit vector
                VVector planeVector2 = new VVector(unitVector.y * planeUnitVector1.z - unitVector.z * planeUnitVector1.y, unitVector.z * planeUnitVector1.x - unitVector.x * planeUnitVector1.z, unitVector.x * planeUnitVector1.y - unitVector.y * planeUnitVector1.x);
                VVector planeUnitVector2 = planeVector2 / planeVector2.Length;

                // line search with start points in the plane. Be sure to define the plane large enough and the search lines far enough to capture all of the healthy tissue between target and source
                for (int i = -200; i < 200; i += 1) // 40 cm should cover the average head, right?
                {
                    for (int j = -200; j < 200; j += 1)
                    {
                        // Get target segment profile
                        VVector startPoint = planePoint + i * planeUnitVector1 + j * planeUnitVector2;
                        VVector endPoint = startPoint + 1000 * unitVector;
                        System.Collections.BitArray segmentStride = new System.Collections.BitArray(1000);

                        SegmentProfile targetProfile = beamTarget.GetSegmentProfile(startPoint, endPoint, segmentStride);

                        // if target is on the line
                        if (targetProfile.Any(p => p.Value))
                        {
                            // find point where target ends and VOI starts
                            VVector targetEndPoint = targetProfile.Last(p => p.Value).Position;
                            System.Collections.BitArray voiStride = new System.Collections.BitArray(1000);

                            SegmentProfile voiProfile = structure.GetSegmentProfile(targetEndPoint, endPoint, voiStride);
                            if (voiProfile.Any(p => p.Value))
                            {
                                VVector structureStartPoint = voiProfile.First(p => p.Value).Position;
                                VVector structureEndPoint = voiProfile.Last(p => p.Value).Position;
                                returnArray[beamCount] += GetDistance3D(structureStartPoint, structureEndPoint) / 1000;
                                // since both the plane grid resolution and the segmentProfile resolution are 1mm the answer should come out in mm^3
                            }
                        }
                        else
                        {
                            continue;
                        }
                    }
                }

                beamCount++;
                
            }

            return returnArray;
        }

        /// <summary>
        /// Calculate the 3D distance between two points.
        /// </summary>
        /// <param name="point1"></param>
        /// <param name="point2"></param>
        /// <returns></returns>
        private double GetDistance3D(VVector point1, VVector point2)
        {
            double dist = Math.Sqrt(Math.Pow((double)point1.x - (double)point2.x, 2) + Math.Pow((double)point1.y - (double)point2.y, 2) + Math.Pow((double)point1.z - (double)point2.z, 2));
            return dist;
        }
        #endregion
    }
}
